provider  "aws"{
  region = "us-east-1"
  access_key = ""
  secret_key = ""
}


resource "aws_eip" "eipp" {
    vpc = true
}



resource "aws_instance" "MyEC2" {
    ami = "ami-0e322da50e0e90e21"
    instance_type = "t2.nano"
    subnet_id = "subnet-00cad1e4acf718370"
}

resource "aws_eip_association" "eip_assoc" {
  instance_id   = aws_instance.MyEC2.id
  allocation_id = aws_eip.eipp.id
}

resource "aws_security_group" "allow_tls" {
  name        = var.sg-names[count.index]
  description = "Allow TLS inbound traffic"
  count = 3
  ingress {
    description      = "chennai_ip"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = [var.ip_list[0]]
    }

      ingress {
        description      = "TLS from VPC"
        from_port        = 5000
        to_port          = 5000
        protocol         = "tcp"
        cidr_blocks      = [var.ip_list[1]]
        }
 } 
