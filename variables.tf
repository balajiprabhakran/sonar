variable "ip_list" {
  type    = list(any)
  default = ["10.1.1.1/32", "10.1.1.2/32"]
}

variable "sg-names" {
  default = ["hello", "hi", "how", "are", "you"]
  type    = list(any)
}
